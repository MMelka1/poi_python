# import numpy as np # import całej biblioteki 'numpy' z funkcjami. dopisnka 'as np' (zwana aliasem)
                     # będzie umożliwiała dowołanie się do biblioteki poprzez 'np.'
                     # importowanie całych bibliotek jest jednak niewygodne z racji
                     #wolniejszego działania kodu.
from scipy.stats import norm  # import jednej, wyszczególnionej funkcji 'norm' z biblioteki scipy.static
                              # zostanie ona lokalnie dodana do skryptu, dzięki czemu program bedzie działał szybciej
                              # funkcja 'norm' - rozkład normalny, pozwala na wygenerowanie losowych punktów(liczb)
from csv import writer       # import fukcji zapisu do pliku csv z biblioteki csv


def generate_points(num_points:int = 2000):  #':int' - typehit - określa moje oczekiwanie co do typu argumentu
    distribution_x = norm(loc=0, scale=20)
    distribution_y = norm(loc=0, scale=200)
    distribution_z = norm(loc=0.2, scale=0.05) # 3 obiekty reprezentujące rozkład prawdopodobieństwa, obiekty są parametryzowane  średnią loc i odchyleniem standardowym scale,z których beda losowane punkty.  loc-= oznacza, że nasz lidar jest umieszczony w przestrzeni x i y w punkcie 0.

    distribution_xx = norm(loc=0, scale=20)
    distribution_yy = norm(loc=0.2, scale=0.05)
    distribution_zz = norm(loc=0, scale=200)

    distribution_xxx = norm(loc=0, scale=200)
    distribution_yyy = norm(loc=0, scale=200)
    distribution_zzz = norm(loc=0, scale=500)

    x = distribution_x.rvs(size=num_points)
    y = distribution_y.rvs(size=num_points)
    z = distribution_z.rvs(size=num_points) # generacja 2000 punktów przy użyciu funkcji 'rvs'
    # Ta finkcja ma dostęp do kontekstu zewnętrzego skrypu ! Widzi jakie dane, obiekty są w głównej części kodu
    xx = distribution_xx.rvs(size=num_points)
    yy = distribution_yy.rvs(size=num_points)
    zz = distribution_zz.rvs(size=num_points)

    xxx = distribution_xxx.rvs(size=num_points)
    yyy = distribution_yyy.rvs(size=num_points)
    zzz = distribution_zzz.rvs(size=num_points)

    points = zip(x,y,z,) #struktura zawierająca koordynaty x y z. Suwak 'zip' spina je w jedną listę

    return points


if __name__ =='__main__': #ta część kodu będzie wykonana tylko wtedy, kiedy uruchamiam ten skrypt. Jeśli ten skrypt bedę chcał wykorrzystać w innym skrypcie jako funckja do generacji punktów to ta część kodu się nie wykona.
    cloud_points = generate_points(2000)
    with open('LidarData.xyz.xyz', 'w', encoding='utf-8', newline='\n') as csvfile: #contect manager: Tworzy plik LidarData.xyz
                                                                                      # do zapisu danych
          csvwriter = writer(csvfile) # obiekt zapisujący
          #csvwriter.writerow('x', 'y', 'z') # opcja dodania nagłówków do generowanego pliku
          #pętle zapsiującą 2000 rekordów do pliku można by było zrealizować w taki sposób:
          # for i in range(num_points) # przy inicjalizacji 'i' jest z automatu równe 0
          #     csvwriter.writerow(x[i],y[i],z[i])
          for p in cloud_points: #p=0
              csvwriter.writerow(p)
# wykonanie lub niewyk. tej części kodu jest spowodowane tym ,że gdy uruchamiam ten skrypt to dostaję z automatu 'name' jako 'main'. Gdy ten skrypt będzie wykorzystany w innym skrypcie jako funkcja, ta część nie zostanie wykonana.